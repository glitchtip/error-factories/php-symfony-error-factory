<?php
namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    public $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route(name="home", path="/")
     */
    public function number(): Response
    {
        \Sentry\configureScope(function (\Sentry\State\Scope $scope): void {
            $scope->setContext('fun', [
                'name' => 'yes'
            ]);
        });
        $this->logger->error('My custom logged error.');
        throw new \RuntimeException('Example exception.');

        return new Response(
            '<html><body>Hello World</body></html>'
        );
    }
}
?>
